@extends('layouts.app')

@section('content')
  <div class="container">
    @if (session('err_msg'))
<p>{{session('err_msg')}}</p>
@endif
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">コメント投稿</div>
                <div class="card-body">

                  <form action="{{ route('comment') }}" method="POST">
                    @csrf
                    <label for="content" class="col-form-label text-md-right">コメント</label><br>
                    <textarea name="content" class="form-control @error('content') is-invalid @enderror" required></textarea>

                    @if ($errors->has('content'))
                    <p>{{ $errors->first('content')}}</p>
                    @endif
                     <input type="hidden" name="id" value="{{$id}}">
                     <div class="col-md-8 offset-md-4">
                      <button type="submit" class="mt-2 btn btn-primary">投稿する</button>
                    </div>
                  </form>

                </div>
                  <a href="/thread/{{$id}}" class="ml-2">記事へ戻る</a>
            </div>
        </div>
  </div>









@endsection
