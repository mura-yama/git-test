@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <form action="/search/result" method="GET">
    @csrf
    <span>キーワード</span>
    <input type="text" name="search">
    <button type="submit">検索</button>
    </form>
  </div>

    <div class="m-3">
      @if (session('err_msg'))
    <p>{{session('err_msg')}}</p>
    @endif
      @if(!empty($threads))
      @foreach($threads as $thread)
    <a href="/thread/{{ $thread->id }}" class="text-dark text-decoration-none">
    <div class="card p-3 mb-1">
      <h4>{{ $thread->title }}</h4>
      <p>{{ $thread->content}}</p>
    </div>
    </a>
    @endforeach
    @endif
    </div>
</div>
@endsection
