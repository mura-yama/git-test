<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Sendmail extends Mailable
{
    use Queueable, SerializesModels;
    private $text;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text)
    {
    $this->text = $text;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this
            ->from('apptest883@gmail.com')
            ->subject('掲示板アプリ本登録案内')
            ->view('mails.user_register')
            ->with([
                'text' => $this->text,
            ]);
    }
}
