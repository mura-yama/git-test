<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Blog;
use App\Models\Comment;

class CommentController extends Controller
{
    //  投稿フォームを作成
    public function showComment($id){
        return view('thread.comment',
    ['id'=>$id]);
    }


    public function storeComment(Request $request){
        $id = $request->id;
        $user_id = Auth::id(); //ユーザーのid
        $str = Str::random();
        $post = new Comment;
        $post->unique_id = $str;
        $post->comment = $request->content;
        $post->user_id = $user_id;
        $post->blog_id =(int)$id; //スレッドのID
        $post->save();
        \Session::flash('err_msg', 'コメントを登録しました');
        return redirect('/thread/'.$id);
    }

}
