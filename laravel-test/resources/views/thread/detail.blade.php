@extends('layouts.app')

@section('content')

<div class="container">
  <div class="blog-container">
@if (session('err_msg'))
  <p>{{session('err_msg')}}</p>
@endif

  <div class="justify-content-center">
      <h2>{{ $user->title }}</h2>
      <span>{{ $user->name}}</span><br>
      <span>{{ $user->blog_create}}</span>
      <div class="text-area">
      <p class="text">{{ $user->content}}</p>
      </div>
    </div>


  <div class="comment-container">
    <div class="btn-area">
      <p>コメント</p>
    <a href="/thread/{{ $user->blog_id }}/comment" class="cmt-btn">コメントする</a>
  </div>
  <div class="comment-field">
    @if(!empty($comments))
    @foreach($comments as $comment)
    <div class="card">
      <div class="card-body">
        <p class="card-text">{{$comment->comment}}</p>
        <div class="d-flex">
          <p>投稿者:{{$comment->name}}</p>
          <p class="ml-2">投稿日:{{$comment->comment_created}}</p>
        </div>
      </div>
    </div>
    @endforeach
    @endif
  </div>
</div>

@endsection
