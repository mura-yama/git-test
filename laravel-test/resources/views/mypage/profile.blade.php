@extends('layouts.app')

@section('content')
 <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">ユーザー情報</div>
                <div class="card-body">
                <p>ユーザーネーム: <span>{{$name}}</span></p>
                <p>メールアドレス <span>{{$email}}</span></p>
                <p>パスワード <span>********</span></p>
                </div>
                <a href="/mypage/profile_edit/{{$id}}" class="text-center">登録情報を編集する</a>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
