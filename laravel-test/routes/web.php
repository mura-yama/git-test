<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


// メール送信
Route::get('/pre_register', 'Auth\RegisterController@showPreRegister')->name('showPreRegister');
Route::post('/pre_register', 'Auth\RegisterController@preRegister')->name('preRegister');
Route::get('/register/{token}', 'Auth\RegisterController@checkRegister')->name('checkRegister');

// 一覧画面を表示
Route::get('/thread', 'BlogController@showList')->name('thread.list')->middleware('auth');
// 登録画面表示
Route::get('/thread/create', 'BlogController@showCreate')->middleware('auth')->name('create');
// 記事登録
Route::post('/thread/store', 'BlogController@exeStore')->middleware('auth')->name('store');
// 詳細を表示
Route::get('/thread/{id}', 'BlogController@showLDetail')->middleware('auth')->name('show');



// 検索用ページ表示
Route::get('/search', 'BlogController@search')->middleware('auth')->name('search');
Route::get('/search/result', 'BlogController@searchResult')->middleware('auth')->name('search.result');



//コメントフォーム表示用
Route::get('/thread/{id}/comment','CommentController@showComment')->middleware('auth')->name('show.comment');
//コメント登録
Route::post('/comment/store','CommentController@storeComment')->middleware('auth')->name('comment');

// マイページ表示
Route::get('/mypage', 'UserController@index')->name('mypage')->middleware('auth');
Route::get('/mypage/profile_edit/{id}', 'UserController@edit')->middleware('auth');
Route::post('/mypage/profile_edit/update', 'UserController@update')->middleware('auth')->name('user.update');
