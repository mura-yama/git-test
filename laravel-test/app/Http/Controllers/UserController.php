<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class UserController extends Controller
{
    public function index()
    {
    $auth = Auth::user();

    return view('mypage.profile',
    ['id' => $auth['id'], 'name' => $auth['name'], 'email'=>Crypt::decryptString($auth['email'])]);
    }

    public function edit($id)
    {
    $auth = Auth::user();
    return view('mypage.profile_edit',[ 'name' => $auth['name'], 'email'=>Crypt::decryptString($auth['email'])]);
    }


    public function update(Request $request)
    {

    $id = Auth::id();
    $auth = User::find($id);

    $form = $request->all();
    $auth->fill(
        [
    'name'=>$form['name'],
    'email'=> Crypt::encryptString($form['email']),
    'password' => Hash::make($form['password'])
        ])->save();
    \Session::flash('err_msg', '更新しました');
    return redirect('/mypage');

    }



}
