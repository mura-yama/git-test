<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommnentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commnents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('unique_id',255)->unique();
            $table->text('comment');
            $table->integer('user_id')->length();
            $table->integer('blog_id')->length();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commnents');
    }
}
