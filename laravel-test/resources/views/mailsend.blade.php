@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">


                <div class="card-body">
                  <h4>送信完了</h4>
                  <p>入力いただいたメールアドレス宛にメールを送信しました。
                    メール記載のURLから本登録を行ってください。</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
