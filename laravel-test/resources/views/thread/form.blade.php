@extends('layouts.app')

@section('content')



  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">記事投稿</div>
                <div class="card-body">
                  <form action="{{route('store')}}" method="POST">
                  @csrf
                    <label for="title" class="col-form-label text-md-right">タイトル</label>
                    <input type="text" name="title" class="form-control @error('title') is-invalid @enderror" required autocomplete="title">
                    @if ($errors->has('title'))
                    <p>{{ $errors->first('title')}}</p>
                    @endif
                    <label for="content" class="col-form-label text-md-right">本文</label><br>
                    <textarea name="content" class="form-control @error('content') is-invalid @enderror" required></textarea>

                    @if ($errors->has('content'))
                    <p>{{ $errors->first('content')}}</p>
                    @endif

                    <div class="col-md-8 offset-md-4">
                      <button type="submit" class="mt-2 btn btn-primary">投稿する</button>
                    </div>
                  </form>
                </div>
            </div>
        </div>
  </div>



@endsection
