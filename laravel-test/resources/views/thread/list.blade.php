@extends('layouts.app')

@section('content')
<div class="container">
  <div class="d-flex justify-content-center">
    <h2>記事一覧</h2>
  </div>
  <div class="d-flex justify-content-center">
    <p class="m-3">登録者数:{{$users_count}}</p>
    <p class="m-3">スレッド数:{{$threads_count}}</p>
  </div>

  @if (session('err_msg'))
  <p>{{session('err_msg')}}</p>
  @endif
  @foreach($users as $user)
  <a href="/thread/{{ $user->blog_id }}" class="text-dark text-decoration-none">
    <div class="card p-3 mb-1">
      <h3>{{ $user->title }}</h3>
      <p>{{ $user->content}}</p>
      <p>{{ $user->name}}</p>
      <p>{{ $user->blog_create}}</p>
    </div>
  </a>
  @endforeach
</div>
@endsection
