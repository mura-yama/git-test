<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Http\Requests\BlogRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    //一覧を表示する
    public function showList()
    {
        $users = DB::table('users')
        ->select('blogs.id as blog_id','name','title','content','blogs.created_at as blog_create')
        ->join('blogs', 'users.id', '=', 'blogs.user_id')
        ->get();

        $threads_count = count($users);
        $users_count = DB::table('users')->select('id')->get();
        $users_count = count($users_count);

        return view('thread.list',
        ['users' => $users,
        'threads_count' => $threads_count,
        'users_count' => $users_count]);
    }

    //詳細表示する
    public function showLDetail($id)
    {
        $user = DB::table('users')
        ->select('blogs.id as blog_id','name','title','content','blogs.created_at as blog_create')
        ->join('blogs', 'users.id', '=', 'blogs.user_id')
        ->where('blogs.id','=',$id)
        ->get();


        $comments = DB::table('comments')
        ->select('comments.comment','comments.created_at as comment_created','users.name')
        ->join('users', 'users.id', '=', 'comments.user_id')
        ->where('comments.blog_id','=',$id)
        ->get();


      $thread = Blog::find($id);
        if(is_null($thread)){
           return redirect(route('thread.list'));
        }

        return view('thread.detail',
        ['user'=>$user[0],'comments'=>$comments]);

    }

    // ブログ投稿フォームを表示
    public function showCreate(){
        return view('thread.form');
    }

    // ブログを登録
    public function exeStore(Request $request){
        $id = Auth::id();
        $str = Str::random();
        $post = new Blog;
        $post->unique_id = $str;
        $post->title = $request->title;
        $post->content = $request->content;
        $post->user_id = $id;
        $post->save();
        return redirect()->route('thread.list');
        \Session::flash('err_msg', 'ブログを登録しました');
    }

    // 検索用画面表示
    public function search(){
        return view('thread.search');
    }


    // 検索結果
    public function searchResult(Request $request){
     $keyword = $request->input('search');
     $query = Blog::query();
        if(!empty($keyword)){
            $query->where('title','like','%'.$keyword.'%')->orWhere('content','like','%'.$keyword.'%');
            $threads = $query->get();

            if(count($threads) == 0){
                \Session::flash('err_msg', '検索結果なし');
                return view('thread.search');
            }else{
                return view('thread.search',
                ['threads' => $threads]);
            }

        }else{
                \Session::flash('err_msg', 'キーワードを入力してください');
            return view('thread.search');
        }
}

}
