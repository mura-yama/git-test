<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Models\PreUser;
use Illuminate\Support\Facades\Mail;
use App\Mail\Sendmail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/thread';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => Crypt::encryptString($data['email']),
            'password' => Hash::make($data['password']),
        ]);
    }


    public function showPreRegister(){
        return view('auth/pre_register');
    }


    protected function emailValidator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
    }

    public function preRegister(Request $request){
        // バリデーション

        $validator = $this->emailValidator($request->all());
        if ($validator->fails()) {
            return view('auth.pre_register');
        }else{
        $user = new PreUser;
        $mail = $request->email;
        $encrypted = Crypt::encryptString($request->email);
        $user->email = $encrypted;
        $token = Str::random();
        $user->token = $token;
        $user->save();
        $text = 'http://127.0.0.1:8000/register/'.$token;
        Mail::to($mail)->send( new Sendmail($text) );
        return view('mailsend');
         }
    }


    public function checkRegister($token){
        $user = DB::table('pre_users')
        ->select('token')
        ->where('token','=',$token)
        ->get();
        if(count($user) > 0){
            return view('auth.register');
        }else{
            return view('auth.pre_register');
        }
    }

}
